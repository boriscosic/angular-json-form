import { Component } from '@angular/core';

@Component({
  selector: 'jf-example',
  template: '<div class="container-fluid"><br /><jf-builder class="form-builder"></jf-builder></div>'
})
export class ExampleComponent {
  constructor() {}
}
