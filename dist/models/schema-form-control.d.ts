import { FormControl } from '@angular/forms';
import { Schema } from './schema';
export declare class SchemaFormControl extends FormControl {
    schema: Schema;
    style: any;
}
