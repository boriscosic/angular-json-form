import { Component } from '@angular/core';
var ExampleComponent = /** @class */ (function () {
    function ExampleComponent() {
    }
    ExampleComponent.decorators = [
        { type: Component, args: [{
                    selector: 'jf-example',
                    template: '<div class="container-fluid"><br /><jf-builder class="form-builder"></jf-builder></div>'
                },] },
    ];
    /** @nocollapse */
    ExampleComponent.ctorParameters = function () { return []; };
    return ExampleComponent;
}());
export { ExampleComponent };
