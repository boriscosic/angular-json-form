import { SchemaFormGroup } from '../../models/schema-form-group';
export declare class ChooserComponent {
    index: number;
    form: SchemaFormGroup;
    schema: Object;
    submitted: boolean;
    keys: (o: {}) => string[];
}
